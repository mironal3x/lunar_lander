﻿using UnityEngine;
using Parse;

public class ParseStorage : MonoBehaviour {

	void Start() {
		Levels.parseDelegate += SaveScore;
	}

	void SaveScore(int level, int difficulty, float score) {
		var scoreObj = new ParseObject("Score");
		scoreObj["level"] = level;
		scoreObj["difficulty"] = difficulty;
		scoreObj["score"] = score;
		scoreObj.SaveAsync();
	}
}
