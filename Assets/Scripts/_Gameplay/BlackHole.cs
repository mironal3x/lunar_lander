﻿using UnityEngine;

public class BlackHole : MonoBehaviour {

	public float attractionStrength;

	void OnTriggerStay2D(Collider2D col) {
		if(Lander.Instance.rb.isKinematic) return;

		if(col.attachedRigidbody != null) {
			var offset = col.attachedRigidbody.transform.position - transform.position;
			offset.z = 0;
			var mag = offset.magnitude;
			if(mag > 0.0001f) {
				col.attachedRigidbody.AddForce(- attractionStrength * offset.normalized / mag, ForceMode2D.Impulse);
			}
		}
	}
}
