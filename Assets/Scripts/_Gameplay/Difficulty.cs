﻿using UnityEngine;
using System;

public class Difficulty : MonoBehaviour {

	public WindZone[] windZones;
	public BlackHole[] blackHoles;

	void Start() {
		Levels.difficultyDelegate += difficulty => {

			var windTemp = Mathf.Clamp(difficulty / 2, 0, windZones.Length - 1);
			var holesTemp = Mathf.Clamp((int)((float)difficulty / 2 - 0.5f), 0, blackHoles.Length - 1);

			                   windZones [windTemp] .gameObject.SetActive(true);
			if(difficulty > 0) blackHoles[holesTemp].gameObject.SetActive(true);

//			switch(difficulty) {
//			case 0: windZones[0].gameObject.SetActive(true);
//				break;
//			case 1: blackHoles[0].gameObject.SetActive(true);
//				break;
//			case 2: windZones[1].gameObject.SetActive(true);
//				break;
//			case 3: blackHoles[1].gameObject.SetActive(true);
//				break;
//			case 4: windZones[2].gameObject.SetActive(true);
//				break;
//			case 5: blackHoles[2].gameObject.SetActive(true);
//				break;
//			case 6: windZones[3].gameObject.SetActive(true);
//				break;
//			case 7: blackHoles[3].gameObject.SetActive(true);
//				break;
//			case 8: windZones[4].gameObject.SetActive(true);
//				break;
//			case 9: blackHoles[4].gameObject.SetActive(true);
//				break;
//			}
		};
	}
}
