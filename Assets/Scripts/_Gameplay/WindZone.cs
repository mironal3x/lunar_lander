﻿using UnityEngine;

public class WindZone : MonoBehaviour {

	public float windForceStrength;
	public Vector2 windForce;

	void OnTriggerStay2D(Collider2D col) {
		if(Lander.Instance.rb.isKinematic) return;

		if(col.attachedRigidbody != null) {
			windForce = new Vector2(Mathf.Cos(Mathf.Deg2Rad * transform.eulerAngles.z) * windForceStrength, Mathf.Sin(Mathf.Deg2Rad * transform.eulerAngles.z)  * windForceStrength);
			col.attachedRigidbody.AddForce(windForce);
		}
	}
}
