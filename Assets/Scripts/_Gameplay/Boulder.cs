﻿using UnityEngine; 
 
public class Boulder : MonoBehaviour { 

	public Sprite[] sprites;
	public float speed;

	Rigidbody2D rb;
	SpriteRenderer spriteRenderer;
	Vector3 endPosition;
	float startX;
	float endPoint;

	float aspectRatio { get { return (float)Screen.width / Screen.height; } }
	float randomY {get { return Random.Range(-2f, 3f); } }

	const float OFFSET = 3f;

	void Start() {
		spriteRenderer = GetComponent<SpriteRenderer>();
		rb = GetComponent<Rigidbody2D>();
		startX = - Camera.main.orthographicSize * aspectRatio - OFFSET;
		endPoint = - startX;
		transform.position = new Vector3(startX, randomY, 0) ;
		Lander.Instance.endDelegate += state => transform.position = new Vector3(startX, randomY, 0) ;
		UI.restartDelegate += () => transform.position = new Vector3(startX, randomY, 0) ;
		Wind.Instance.Subscribe(rb);
	}

	void OnCollisionEnter2D (Collision2D col) {
		var lander = col.transform.GetComponent<Lander>();
		if(lander) {
			lander.AddDamage(rb.velocity.magnitude);
		}
	}

	void Update () { 
		if(Lander.Instance.rb.isKinematic) {
			rb.velocity = Vector2.zero;
			return;
		}

		if(transform.position.x > endPoint) {
			transform.position = new Vector3(startX, randomY, 0) ;
			rb.velocity = Vector2.zero;
			spriteRenderer.sprite = sprites.Random();
		}
		endPosition = new Vector3(endPoint, transform.position.y, transform.position.z);
		transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
	}
} 
 