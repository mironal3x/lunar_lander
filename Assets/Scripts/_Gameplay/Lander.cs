﻿using UnityEngine;
using System;

public class Lander : Singleton<Lander> {
	public enum State { Successful, Failed }

	public Action<State> endDelegate;
	public Action engineFireDelegate;

	[HideInInspector] 
	public Rigidbody2D rb;
	public SpriteRenderer damageBar;
	public ParticleSystem explosion;
	public Engine rightEngine;
	public Engine leftEngine;
	public TextMesh percentage;

	public float health;
	public float damage;

	Vector3 startPoint;
	float prevFrameVeloMag;
	int frames;
	bool collidedRecently;

	const int FRAMES_TO_SKIP = 10;

	public float remainingHealth { 
		get {
			return health - damage;
		}
	}

	protected override void Awake() {
		base.Awake();
		startPoint = transform.position;
		rb = GetComponent<Rigidbody2D>();
		Wind.Instance.Subscribe(rb);
	}

	void Start () {
		Application.targetFrameRate = 60;
	}
	
	void FixedUpdate () {
		if(Input.GetKey(KeyCode.LeftArrow)) {
			if(!rb.isKinematic) {
				leftEngine.FireEngine();
				engineFireDelegate.Fire();
			}
		}
		if(Input.GetKey(KeyCode.RightArrow)) {
			if(!rb.isKinematic) {
				rightEngine.FireEngine();
				engineFireDelegate.Fire();
			}
		}
	}

	void LateUpdate() {
		prevFrameVeloMag = rb.velocity.magnitude;
		if(collidedRecently && frames < FRAMES_TO_SKIP) {
			frames++;
			return;
		}
		frames = 0;
		collidedRecently = false;
	}

	void OnCollisionEnter2D(Collision2D col) {
		if(collidedRecently) return;
		collidedRecently = true;

		if(prevFrameVeloMag > .5f) {
			AddDamage(prevFrameVeloMag);
		} 
	}

	void Update() {
		if(remainingHealth > 0 && rb.velocity.magnitude < 0.001f) 
			CheckLanding();
	}

	void OnBecameInvisible() {
		if(!Application.isEditor) {
			AddDamage(health + 1);
		}
	}

	void CheckLanding() {
		if(rb.isKinematic) return;

		var hitLeftEngine = Physics2D.Raycast(leftEngine.transform.position, transform.up * -0.1f, 1f, 1 << 9);
		var hitRightEngine = Physics2D.Raycast(rightEngine.transform.position, transform.up * -0.1f, 1f, 1 << 9);
		var hitHeadReversed = Physics2D.Raycast(transform.position, transform.up * 0.4f, 1f, 1 << 0);
	
		Debug.DrawRay(leftEngine.transform.position, transform.up * -0.1f, Color.red, 1f);
		Debug.DrawRay(rightEngine.transform.position, transform.up * -0.1f, Color.yellow, 1f);
		Debug.DrawRay(transform.position, transform.up * 0.4f, Color.blue, 1f);

		bool landedOn1 = false, landedOn2 = false;
		landedOn1 |= hitLeftEngine.transform != null && hitLeftEngine.transform.GetComponent<BoxCollider2D>();
		landedOn2 |= hitRightEngine.transform != null && hitRightEngine.transform.GetComponent<BoxCollider2D>();

		if(hitHeadReversed.transform != null && hitHeadReversed.transform != transform && !hitHeadReversed.transform.GetComponent<WindZone>() && rb.velocity.magnitude < 0.0001f) {
			endDelegate.Fire(State.Failed);
		}

		if(landedOn1 && landedOn2) {
			endDelegate.Fire(State.Successful);
		}
	}

	public void Reset() {
		transform.position = startPoint;
		transform.rotation = Quaternion.identity;
		rb.isKinematic = true;
		damageBar.transform.localScale = Vector3.zero;
		damage = 0;
		percentage.text = "100%";
	}

	public void AddDamage(float dmg) {
		damage += dmg;
		damageBar.transform.localScale = new Vector3(damage / health, 1, 1);
		percentage.text = (int)(100 - (damage / health) * 100) + "%";
		if(damage / health >= 1) {
			percentage.text = "0%";
			damageBar.transform.localScale = Vector3.one;
			Die();
		}
	}

	void Die() {
		var boom = UnityEngine.Object.Instantiate (explosion, transform.position, Quaternion.identity);
		gameObject.SetActive(false);
		endDelegate.Fire(State.Failed);
		boom.Destroy(5f);
	}
}
