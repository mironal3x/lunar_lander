﻿using UnityEngine;

public class Engine : MonoBehaviour {

	public ParticleSystem fire;
	public float speed;

	Rigidbody2D rb; 

	void Awake() {
		rb = GetComponent<Rigidbody2D>();
		Wind.Instance.Subscribe(rb);
	}

	public void FireEngine () {
		rb.AddForce(transform.parent.up * speed);
		if(!fire.isPlaying) {
			fire.Play();
		}
	}

	void OnDrawGizmos() {
		Gizmos.DrawLine(transform.position + transform.parent.up, transform.position + 5 * (transform.parent.up));
	}

}
