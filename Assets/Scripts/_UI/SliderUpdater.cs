﻿using UnityEngine;
using UnityEngine.UI;
using System.Reflection;

public class SliderUpdater : MonoBehaviour {

	public string storageName;

	Slider slider;

	void OnEnable() {
		if(slider == null) slider = GetComponent<Slider>();

		var property = typeof(LocalStorage).GetProperty(storageName, BindingFlags.Static | BindingFlags.Public);
		var propertyValue = property.GetValue(null, null);

		slider.value = (float) propertyValue;
	}

}
